# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Run npm install
* To start project run: npm start
* To run all tests: npm run tests

### Project description ###

* Project has list component whis uses table and loader. List component fetches data using effects.
* All logic about fetching data is in src/utils/fetch_data.ts
* Some helpers to fetch data or sort data you can find in src/utils/utils.ts
* Lis component also uses cellRenderers to handle custom data types
* We can provide withData, e.g. [ 'homeworld' ] to fetch dependencies from another resource
* As for now details module uses also list component, in future we could add some css styled cards
* Table component has sort handler but for now it is turned off, because it's better to implement sort logic on server side

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact