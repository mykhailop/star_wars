import React from 'react';

import { listCellRenderers } from 'modules/shared/list_cell_renderers';
import Details from 'components/details/details';

const fetchConfig = {
    resource: 'species',
    columns: [ 'name', 'images', 'designation', 'language', 'homeworld', 'people', 'films' ],
    withData: [ 'homeworld', 'people', 'films' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
    homeworld: listCellRenderers.externalCell,
    people: listCellRenderers.externalCell,
    films: listCellRenderers.externalCell,
};

const SpeciesDetails = () => {
    return (<><Details {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default SpeciesDetails;
