import React from 'react';

import { listCellRenderers } from 'modules/shared/list_cell_renderers';
import List from 'components/list/list';

const fetchConfig = {
    resource: 'people',
    columns: [ 'name', 'images' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
};

const PeopleList = () => {
    return (<><List {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default PeopleList;
