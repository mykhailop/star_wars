import React from 'react';

import { listCellRenderers } from 'modules/shared/list_cell_renderers';
import Details from 'components/details/details';

const fetchConfig = {
    resource: 'people',
    columns: [ 'name', 'images', 'homeworld', 'vehicles', 'starships', 'species' ],
    withData: [ 'homeworld', 'films', 'vehicles', 'starships', 'species' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
    homeworld: listCellRenderers.externalCell,
    films: listCellRenderers.externalCell,
    vehicles: listCellRenderers.externalCell,
    starships: listCellRenderers.externalCell,
    species: listCellRenderers.externalCell,
};

const PeopleDetails = () => {
    return (<><Details {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default PeopleDetails;
