import React from 'react';

import { listCellRenderers } from 'modules/shared/list_cell_renderers';
import List from 'components/list/list';

const fetchConfig = {
    resource: 'vehicles',
    columns: [ 'name', 'images', 'pilots', 'crew', 'vehicle_class' ],
    withData: [ 'pilots' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
    pilots: listCellRenderers.externalCell,
};

const VehiclesList = () => {
    return (<><List {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default VehiclesList;
