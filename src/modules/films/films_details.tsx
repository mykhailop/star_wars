import React from 'react';

import { listCellRenderers } from 'modules/shared/list_cell_renderers';
import Details from 'components/details/details';

const fetchConfig = {
    resource: 'films',
    columns: [ 'title', 'images', 'release_date', 'director', 'planets', 'characters' ],
    withData: [ 'planets', 'characters' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
    planets: listCellRenderers.externalCell,
    characters: listCellRenderers.externalCell,
};


const FilmsDetails = () => {
    return (<><Details {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default FilmsDetails;
