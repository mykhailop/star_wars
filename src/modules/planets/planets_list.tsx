import React from 'react';
import List from 'components/list/list';
import { listCellRenderers } from 'modules/shared/list_cell_renderers';

const fetchConfig = {
    resource: 'planets',
    columns: [ 'name', 'images', 'population', 'residents' ],
    withData: [ 'residents' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
    residents: listCellRenderers.externalCell,
};

const PlanetsList = () => {
    return (<><List {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default PlanetsList;
