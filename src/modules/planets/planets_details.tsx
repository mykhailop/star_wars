import React from 'react';

import { listCellRenderers } from 'modules/shared/list_cell_renderers';
import Details from 'components/details/details';

const fetchConfig = {
    resource: 'planets',
    columns: [ 'name', 'images', 'population', 'residents' ],
    withData: [ 'residents' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
    residents: listCellRenderers.externalCell,
};

const PlanetsDetails = () => {
    return (<><Details {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default PlanetsDetails;
