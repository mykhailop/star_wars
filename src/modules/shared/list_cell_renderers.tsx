import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { compact } from 'lodash';

import { BodyConfig, ColumnsConfig, TableConfigProps } from 'interfaces/interfaces.d';
import { getIdFromUrl, getDataFromUrl } from 'utils/utils';
import Image from 'components/image/image_component';

const StyledUl = styled.ul`margin: 0;`;

const listCellRenderers = {
    images: ({ resource }: TableConfigProps) => {
        return (bodyEntry: BodyConfig, columnsEntry: ColumnsConfig) => {
            const rowId = getIdFromUrl(bodyEntry.url);

            return (
                <Link to={`/${resource}/${rowId}`}>
                    <Image src={`/img/${resource}_${rowId}.png`} />
                </Link>
            );
        };
    },
    externalCell: (bodyEntry: BodyConfig, columnsEntry: ColumnsConfig) => {
        const currentEntry = bodyEntry[columnsEntry.id];

        const wrappedEntry = compact(
            Array.isArray(currentEntry) ? currentEntry : [ currentEntry ]
        );

        return (
            <StyledUl>
                { wrappedEntry.map(entry => {
                    const [ resource, id ] = getDataFromUrl(entry.url);

                    return (<li key={entry.url}><Link to={`/${resource}/${id}`}>{entry.name || entry.title}</Link></li>);
                }) }
            </StyledUl>
        );
    },
};

export { listCellRenderers };
