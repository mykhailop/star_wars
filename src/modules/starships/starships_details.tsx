import React from 'react';

import { listCellRenderers } from 'modules/shared/list_cell_renderers';
import Details from 'components/details/details';

const fetchConfig = {
    resource: 'starships',
    columns: [ 'name', 'images', 'pilots', 'crew', 'starship_class' ],
    withData: [ 'pilots' ],
};

const cellRenderers = {
    images: listCellRenderers.images(fetchConfig),
    pilots: listCellRenderers.externalCell,
};

const StarshipsDetails = () => {
    return (<><Details {...fetchConfig} cellRenderers={cellRenderers} /></>);
};

export default StarshipsDetails;
