import React from 'react';
import styled from 'styled-components'
import { Switch, Route } from 'react-router-dom';
// People
import PeopleList from 'modules/people/people_list';
import PeopleDetails from 'modules/people/people_details';
// Planets
import PlanetsList from 'modules/planets/planets_list';
import PlanetsDetails from 'modules/planets/planets_details';
// Starships
import StarshipsList from 'modules/starships/starships_list';
import StarshipsDetails from 'modules/starships/starships_details';
// Vehicles
import VehiclesList from 'modules/vehicles/vehicles_list';
import VehiclesDetails from 'modules/vehicles/vehicles_details';
// Species
import SpeciesList from 'modules/species/species_list';
import SpeciesDetails from 'modules/species/species_details';
// Films
import FilmsList from 'modules/films/films_list';
import FilmsDetails from 'modules/films/films_details';

// calc width 100% - menu width
const StyledContent = styled.div`
    font-size: 14px;
    width: calc(100% - 240px);
`;

const PaddingDiv = styled.div`padding: 5rem;`;

function Content() {
    return (
        <StyledContent>
            <PaddingDiv>
                <Switch>
                    <Route exact path="/" component={PeopleList} />

                    <Route exact path="/people" component={PeopleList} />
                    <Route exact path="/people/:id" component={PeopleDetails} />

                    <Route exact path="/planets" component={PlanetsList} />
                    <Route exact path="/planets/:id" component={PlanetsDetails} />

                    <Route exact path="/starships" component={StarshipsList} />
                    <Route exact path="/starships/:id" component={StarshipsDetails} />

                    <Route exact path="/vehicles" component={VehiclesList} />
                    <Route exact path="/vehicles/:id" component={VehiclesDetails} />

                    <Route exact path="/species" component={SpeciesList} />
                    <Route exact path="/species/:id" component={SpeciesDetails} />

                    <Route exact path="/films" component={FilmsList} />
                    <Route exact path="/films/:id" component={FilmsDetails} />
                </Switch>
            </PaddingDiv>
        </StyledContent>
    );
}

export default Content;
