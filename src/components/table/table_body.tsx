import React from 'react';
import { isFunction } from 'lodash';

import { BodyConfig, ColumnsConfig, CellRenderers } from 'interfaces/interfaces.d';
import BodyCell from './partials/body_cell';

interface TableBodyProps {
    body: BodyConfig[];
    columns: ColumnsConfig[];
    pinnedColumn?: string;
    cellRenderers?: CellRenderers;
}

function TableBody({ body, columns, pinnedColumn, cellRenderers }: TableBodyProps) {

    return (
        <>
            { (body as BodyConfig[]).map((bodyEntry: BodyConfig, index: number) => {
                return (
                    <React.Fragment key={index}>
                        { (columns as ColumnsConfig[]).map((columnEntry: ColumnsConfig) => {
                            const renderer = cellRenderers && cellRenderers[columnEntry.id];
                            // define cell renderer and pass main data
                            const cellRenderer = isFunction(renderer) ? () => {
                                return renderer(bodyEntry, columnEntry);
                            } : undefined;

                            return (
                                <BodyCell
                                    key={columnEntry.id}
                                    column={columnEntry.id}
                                    text={bodyEntry[columnEntry.id]}
                                    pinnedColumn={pinnedColumn}
                                    cellRenderer={cellRenderer}
                                />
                            );
                        }) }
                    </React.Fragment>
                );
            }) }
        </>
    );
}

export default React.memo(TableBody);
