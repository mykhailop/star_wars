export const pinnedStyles = `
    font-weight: bold;
    position: sticky;
    right: 0rem;
`;

export const commonCellStyles = `
    border: 1px solid #bbb9b9;
    padding: 1rem;
    display: flex;
    align-items: center;
    border-radius: 0.5rem;
`;

