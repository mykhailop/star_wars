import React from 'react';
import { isFunction } from 'lodash';
import styled from 'styled-components';
import { Pin } from '@styled-icons/boxicons-solid';
import { ArrowheadDownOutline, ArrowheadUpOutline } from '@styled-icons/evaicons-outline';

import { HeaderCellProps } from 'interfaces/interfaces.d';
import { pinnedStyles, commonCellStyles } from './common';

const StyledTh = styled.div`
    justify-content: center;
    background: #f1f1f1;
    text-transform: capitalize;
    ${commonCellStyles}
    padding: 0.5rem;
    border: none;
    background: whitesmoke;
    border-bottom: 2px solid gray;
    font-weight: bold;
    ${({ hasClickHandler, columnEntry }: HeaderCellProps) => hasClickHandler ? `:hover { cursor: pointer; }` : ''}
    ${({ columnEntry, pinnedColumn }: HeaderCellProps) => columnEntry.id === pinnedColumn ? `${pinnedStyles}` : ''}
`;

const StyledPin = styled(Pin)`
    transform: rotate(-45deg);
    color: gray;
    :hover {
        color: red;
        cursor: pointer;
    }
`;

const StyledSpan = styled.span`margin: 0.5rem;`;
const StyledPinContainer = styled.span`min-width: 1rem; width: 1rem;`;

const SortIconsMap = {
    asc: ArrowheadDownOutline,
    desc: ArrowheadUpOutline,
};

function HeaderCell({ clickHandler, columnEntry, sort, pinnedColumn, pinHandler }: HeaderCellProps) {
    const sortForColumn = sort && sort[columnEntry.id];
    const SortIcon = sortForColumn && SortIconsMap[sortForColumn];

    return (
        <StyledTh
            columnEntry={columnEntry}
            pinnedColumn={pinnedColumn}
            hasClickHandler={isFunction(clickHandler)}
            onClick={() => {
                if (isFunction(clickHandler)) {
                    clickHandler(columnEntry.id);
                }
            }}
        >
            <StyledPinContainer>
                { pinHandler && pinnedColumn !== columnEntry.id ? (
                    <StyledPin size="18" onClick={(event) => {
                        event.stopPropagation();
                        pinHandler(columnEntry.id)
                    }} />
                ) : <Pin size="18" /> }
            </StyledPinContainer>

            <StyledSpan>{ columnEntry.translation || columnEntry.id }</StyledSpan>

            { sortForColumn && SortIcon ? <SortIcon size="18" /> : null }

        </StyledTh>
    );
}

export default React.memo(HeaderCell);
