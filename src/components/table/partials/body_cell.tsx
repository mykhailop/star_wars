import React from 'react';
import styled from 'styled-components';
import { isFunction } from 'lodash';

import { pinnedStyles, commonCellStyles } from './common';

interface BodyCellProps {
    column: string;
    text?: string;
    pinnedColumn?: string;
    cellRenderer?: () => JSX.Element;
}

const StyledBodyCell = styled.div`
    background: white;
    ${commonCellStyles}
    ${({ pinnedColumn, column }: BodyCellProps) => pinnedColumn === column ? `${pinnedStyles}` : ''}
`;

function BodyCell({ text, column, pinnedColumn, cellRenderer }: BodyCellProps) {
	const defaultValue = text || column;

    return (
        <StyledBodyCell pinnedColumn={pinnedColumn} column={column}>
        	{ isFunction(cellRenderer) ? cellRenderer() : defaultValue }
        </StyledBodyCell>
    );
}

export default React.memo(BodyCell);
