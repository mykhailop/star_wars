import React from 'react';
import { isObject, isFunction } from 'lodash';
import styled from 'styled-components';
import Header from './table_header';
import Body from './table_body';

import { TableConfig, SortDirection, ColumnsConfig } from 'interfaces/interfaces.d';

interface TableProps {
    columns: ColumnsConfig[];
}

const Table = styled.div`
    display: grid;
    align-content: stretch;
    align-items: stretch;
    justify-items: stretch;
    min-height: 100px;
    width: 100%;
    grid-gap: 20px;
    margin-bottom: 1rem;
    overflow-x: auto;
    ${({ columns }: TableProps) => `grid-template-columns: ${columns.map(entry => entry.width || 'auto').join(' ')}` }
`;

const getDirection = (currentDirection?: SortDirection): SortDirection => {
    return currentDirection === 'asc' ? 'desc' : 'asc';
};

function TableComponent({ body, columns, pinnedColumn, pinHandler, sortHandler, sort, cellRenderers }: TableConfig) {
    const modifiedColumns = columns.filter(entry => entry.id !== pinnedColumn);
    const pinnedEntries = columns.filter(entry => entry.id === pinnedColumn);

    // pinned column should be always as last one
    const tableColumns = [ ...modifiedColumns, ...pinnedEntries ];

    const headerCellClickHandler = isFunction(sortHandler) ? (column: string) => {
        const currentDirection = isObject(sort) ? sort[column] : undefined;

        sortHandler(column, getDirection(currentDirection));
    } : undefined;

    return (
        <div>
            <Table columns={tableColumns}>
                { /* we could add cell renderers to header in future */ }
                <Header
                    columns={tableColumns}
                    sort={sort}
                    pinnedColumn={pinnedColumn}
                    clickHandler={headerCellClickHandler}
                    pinHandler={pinHandler}
                />
                <Body
                    columns={tableColumns}
                    body={body}
                    pinnedColumn={pinnedColumn}
                    cellRenderers={cellRenderers}
                />
            </Table>
        </div>
    );
}

export default TableComponent;
