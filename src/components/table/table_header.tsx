import React from 'react';

import { ColumnsConfig, SortConfig } from 'interfaces/interfaces.d';
import HeaderCell from './partials/header_cell';

interface TableHeaderProps {
    columns: ColumnsConfig[];
    sort?: SortConfig;
    pinnedColumn?: string;
    clickHandler?: (column: string) => void;
    pinHandler?: (column: string) => void;
}

function TableHeader({ columns, sort, clickHandler, pinnedColumn, pinHandler }: TableHeaderProps) {
    return (
        <>
            { (columns as ColumnsConfig[]).map((columnEntry: ColumnsConfig) => {
                return (
                    <HeaderCell
                        key={columnEntry.id}
                        clickHandler={clickHandler}
                        columnEntry={columnEntry}
                        pinnedColumn={pinnedColumn}
                        sort={sort}
                        pinHandler={pinHandler}
                    />
                );
            }) }
        </>
    );
}

export default React.memo(TableHeader);
