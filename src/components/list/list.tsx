import React, { useState, useEffect } from 'react';
import Header from 'components/header/header';
import Table from 'components/table/table';
import { getTableConfig } from 'utils/fetch_data';
import { TableConfig, TableConfigProps } from 'interfaces/interfaces.d';
import Pagination from 'components/pagination/pagination';
import Loader from 'components/loader/loader';

/*
    list component has default sort by first column
    we also use here table with pagination and loader component 
    this component has mocked sortHandler with setTimeout
*/

function List({ resource, columns, withData, cellRenderers, config }: TableConfigProps) {
    const defaultState: TableConfig = { body: [], columns: [] };

    const [ tableConfig, setTableConfig ] = useState(config || defaultState);
    const [ isLoading, setIsLoading ] = useState(true);
    const [ pinnedColumn, setPinnedColumn ] = useState();
 
    useEffect(() => {
        const fetchData = async () => {
            if (!columns) {
                return;
            }

            const fetchedConfig = await getTableConfig({ resource, columns, withData });

            setTableConfig(fetchedConfig);

            setIsLoading(false);
        };

        if (!config) {
            fetchData();
        }
    }, []);

    if (!columns && !config) {
        return null;
    }

    return (<>
        <Header text={`${resource} list`} />

        <Loader isLoading={isLoading}>
            <>
                <Table
                    body={tableConfig.body}
                    columns={tableConfig.columns}
                    sort={tableConfig.sort}
                    pinnedColumn={pinnedColumn}
                    cellRenderers={cellRenderers}
                    pinHandler={setPinnedColumn}
                />
                { tableConfig?.pagination ?

                    <Pagination
                        { ...tableConfig?.pagination }
                        onPageChange={async (page: number) => {
                            setIsLoading(true);
                            if (!columns) {
                                return;
                            }

                            // TODO add page number to url so user will have same page
                            // with results after f5
                            const fetchedConfig = await getTableConfig({
                                resource: `${resource}/?page=${page}`,
                                columns,
                                withData,
                            });

                            setTableConfig(fetchedConfig);

                            setIsLoading(false);
                        }}
                    />

                : null }
                
            </>
        </Loader>
    </>);
}

export default List;
