import React from 'react';
import styled from 'styled-components'

const StyledHeader = styled.h1`
	text-transform: capitalize;
`;

function Header({ text }: { text: string }) {
    return (
    	<StyledHeader>{ text }</StyledHeader>
    );
}

export default Header;
