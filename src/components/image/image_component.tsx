import React, { useState } from 'react';

function ImageComponent({ src }) {
    const [ isFailed, setIsFailed ] = useState(false);

    const defaultSize = 200;

    return (<>
        { isFailed ?
            <img src="/img/empty_img.jpg" width={defaultSize} />
        : <img src={src} width={defaultSize} onError={() => { setIsFailed(true) }} /> }
    </>);
}

export default ImageComponent;
