import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Header from 'components/header/header';
import Table from 'components/table/table';
import Loader from 'components/loader/loader';
import { getTableConfigForRow } from 'utils/fetch_data';
import { TableConfig, TableConfigProps } from 'interfaces/interfaces.d';

/*
    list component has default sort by first column
    we also use here table with loader component
    this component has mocked sortHandler with setTimeout
*/

function Details({ resource, columns, cellRenderers, config, withData }: TableConfigProps) {
    const defaultState: TableConfig = { body: [], columns: [] };

    const [ tableConfig, setTableConfig ] = useState(config || defaultState);
    const [ isLoading, setIsLoading ] = useState(true);
    const [ pinnedColumn, setPinnedColumn ] = useState();

    const { id } = useParams();
 
    useEffect(() => {
        const fetchData = async () => {
            if (!columns) {
                return;
            }

            const fetchedConfig = await getTableConfigForRow({ resource, columns, withData, id });

            setTableConfig(fetchedConfig);

            setIsLoading(false);
        };

        if (!config) {
            fetchData();
        }
    }, []);

    if (!columns && !config) {
        return null;
    }

    return (<>
        <Header text={`About one of ${resource}`} />

        <Loader isLoading={isLoading}>
            <Table
                {...tableConfig}
                pinnedColumn={pinnedColumn}
                pinHandler={setPinnedColumn}
                cellRenderers={cellRenderers}
            />
        </Loader>
    </>);
}

export default Details;
