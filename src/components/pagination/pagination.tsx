import React from 'react';
import styled from 'styled-components'
import { LeftArrowAlt, RightArrowAlt } from '@styled-icons/boxicons-solid';

import { PaginationConfig } from 'interfaces/interfaces.d';

const getDisabledStyles = ({ disabled }: { disabled: Boolean }) => {
    return disabled ? 'opacity: 0.5; cursor: not-allowed;' : `
        :hover {
            cursor: pointer;
            color: black;
            box-shadow: 1px 2px 3px 0px grey;
        }`;
};

const StyledDiv = styled.div`
    display: flex;
    align-items: center;
    background: #dedede;
    border-bottom: 3px solid #989898;
    padding: 0.5rem;
    justify-content: flex-end;
    font-size: 16px;
    border-radius: 0.5rem;

    > div {
        margin: 0.5rem;
    }
`;

const StyledButton = styled.div`
    background: white;
    color: grey;
    min-width: 2rem;
    min-height: 2rem;
    border: 1px solid #adacac;
    border-radius: 0.5rem;

    ${getDisabledStyles}
`;

const StyledSpan = styled.span`font-weight: bold`;

const getLastPage = (totalItems: number, perPage: number) => {
    return Math.ceil(totalItems / perPage);
};

type PageType = number | null;

const getCurrentPage = (prevPage: PageType, nextPage: PageType): PageType => {
    if (prevPage) {
        return prevPage + 1;
    } else if (nextPage) {
        return nextPage - 1;
    }

    return null;
};

const Pagination = ({ totalItems, nextPage, prevPage, perPage, onPageChange }: PaginationConfig): JSX.Element | null => {
    const currentPage = getCurrentPage(prevPage, nextPage);

    if (!currentPage) {
        console.error('Something is wrong with pagination data structure. Lack of previous and next pages');
        return null;
    }

    return (
        <StyledDiv>
            <div>Items: <StyledSpan>{totalItems}</StyledSpan>, </div>
            <div>page <StyledSpan>{currentPage}</StyledSpan> of <StyledSpan>{getLastPage(totalItems, perPage)}</StyledSpan></div>
            <StyledButton
                disabled={!prevPage}
                onClick={() => {
                    if (!prevPage || !onPageChange) {
                        return;
                    }

                    onPageChange(prevPage);
                }}
            >
                <LeftArrowAlt size="32" />
            </StyledButton>
            <StyledButton
                disabled={!nextPage}
                onClick={() => {
                    if (!nextPage || !onPageChange) {
                        return;
                    }

                    onPageChange(nextPage);
                }}
            >
                <RightArrowAlt size="32" />
            </StyledButton>
        </StyledDiv>
    );
};

export default React.memo(Pagination);
