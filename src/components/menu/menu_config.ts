const buttonsConfig = [
    { id: 'people', href: '/people' },
    { id: 'vehicles', href: '/vehicles' },
    { id: 'starships', href: '/starships' },
    { id: 'planets', href: '/planets' },
];

export { buttonsConfig };

export default buttonsConfig;
