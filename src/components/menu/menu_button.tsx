import React from 'react';
import styled from 'styled-components'

import { Link } from 'react-router-dom';

const StyledReactLink = styled(Link)`
    color: white;
    font-weight: bold;
    margin: 1rem 0;
    background: #353535;
    border: 1px solid white;
    padding: 0.5rem;
    box-shadow: 1px 1px 4px 0px white;
    text-transform: capitalize;
    text-decoration: none;
    :hover {
        box-shadow: 2px 2px 4px 1px white;
        cursor: pointer;
    }
`;

function Button({ token, href }: { token: string, href: string }) {
    return (
    	<StyledReactLink to={href}>{token}</StyledReactLink>
    );
}

export default Button;
