import React from 'react';
import styled from 'styled-components'

import { buttonsConfig } from './menu_config';
import MenuButton from './menu_button';

const StyledMenu = styled.div`
    width: 240px;
    min-width: 240px;
    position: sticky;
    left: 0;
    top: 0;
    background: black;
    height: 100vh;
`;

const MenuContainer = styled.div`
    padding: 1rem;
    display: flex;
    flex-direction: column;
`;

function Menu() {
    if (!Array.isArray(buttonsConfig)) {
        console.error('Wrong config of menu buttons. Please check structure in components/menu/menu_config.ts');
        return null;
    }

    return (
        <StyledMenu>
            <MenuContainer>
                <img height="170" width="210" src="/img/starwars.png" />
                { buttonsConfig.map(entry => (<MenuButton key={entry.id} token={entry.id} href={entry.href} />)) }
            </MenuContainer>
        </StyledMenu>
    );
}

export default Menu;
