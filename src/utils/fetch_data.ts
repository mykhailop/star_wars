import { intersection, uniq, flatten, mapValues, compact } from 'lodash';

import {
    TableConfig, BodyConfig, APIResponse, GetTableConfigForRowProps,
    GetTableConfigProps, GetBodyWithDependenciesProps,
} from '../interfaces/interfaces.d';

import Utils from './utils';

/*
 *  fetch data from desired resource and return response
 */
const fetchData = async (resource: string): Promise<APIResponse> => {
    const response = await fetch(`https://swapi.dev/api/${resource}`);
    const responseJSON = await response.json();

    return responseJSON;
};

/*
 *  because of swapi structure we don't have possibility to send one request
 *  to get e.g. 1,3,7 elements with one request, so we could fetch all elements
 *  or just only one, with another API it should be refactored to send only one request
 */
const getSingleConfig = async (resource: string, id: string): Promise<BodyConfig> => {
    const response = await fetch(`https://swapi.dev/api/${resource}/${id}`);
    const responseJSON = await response.json();

    return responseJSON;
};

export { getSingleConfig };


/*
 *  get config for table fron desired resource,
 *  e.g. /api/people or /api/planets - get whole list
 *  as for now turn on sort by first columns by default
 */
const getTableConfig = async ({ resource, columns, withData }: GetTableConfigProps): Promise<TableConfig> => {
    const data = await fetchData(resource);  

    const body = await getBodyWithDependencies({ body: data.results, columns, withData });

    const columnsConfig = Utils.getColumns(columns);

    const firstColumn = columns[0];

    return {
        columns: columnsConfig,
        sort: { [firstColumn]: 'asc' },
        // by default we should sort all data from server
        body: Utils.getSorted({ column: firstColumn, body, direction: 'asc' }),
        pagination: Utils.getPaginationData(data),
    };
};

export { getTableConfig };


/*
 *  get config for table for one exact id: api/resource/:id
 */
const getTableConfigForRow = async ({ resource, columns, withData, id }: GetTableConfigForRowProps): Promise<TableConfig> => {
    const data = await getSingleConfig(resource, id);
    const firstColumn = columns[0];

    const body = await getBodyWithDependencies({ body: [ data ], columns, withData });

    return {
        columns:  Utils.getColumns(columns),
        sort: { [firstColumn]: 'asc' },
        // by default we should sort all data from server
        body: Utils.getSorted({ column: firstColumn, body, direction: 'asc' }),
    };
};

export { getTableConfigForRow };


/*
 * Transform body entry, e.g. homeworld: ['http://swapi.dev/api/planets/3/'] to
 * { name: 'Tatooine', population: '532234', ... }
 *
 * 1. Collect all desired entries
 * 2. Make uniq collection to no send unnecessary requests
 * 3. Change value in body entry by new fetched entry
 */
const getBodyWithDependencies = async ({ body, columns, withData }: GetBodyWithDependenciesProps): Promise<BodyConfig[]> => {
    const availableWithData = intersection(columns, withData || []);

    if (!availableWithData.length) {
        return body;
    }

    const fetchedValues = await Promise.all(availableWithData.map(async (column) => {
        const newValue = compact(uniq(
            flatten(body.map(entry => entry[column]))
        ));

        const columnValue = await Promise.all(newValue.map(async (url) => {
            const [ resource, id ] = Utils.getDataFromUrl(url);

            return await getSingleConfig(resource, id);
        }));

        return { [column]: columnValue };
    }));

    return body.map((entry) => {
        return mapValues(entry, (v, key) => {
            return Utils.mapBodyEntryToFetched(v, key, availableWithData, fetchedValues);
        });
    });
};

export { getBodyWithDependencies };
