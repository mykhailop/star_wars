import { orderBy, isString } from 'lodash';
import { SortDirection, BodyConfig, APIResponse, PaginationConfig } from '../interfaces/interfaces.d';

/*
 * traverse response to pagination data format
 */
const getPaginationData = (data: APIResponse): PaginationConfig => {

    const getPage = (prevOrNext: string | null) => {
        if (!isString(prevOrNext)) {
            return null;
        }

        const splitted = prevOrNext.split('?page=');

        return parseInt(splitted[1], 10);
    };

    return {
        totalItems: data.count,
        nextPage: getPage(data.next),
        prevPage: getPage(data.previous),
        perPage: 10, // data.results.length,
    }
};

export { getPaginationData };

/*
 * now the only way to get some id from API is
 * to get id from url, like http://swapi.dev/api/people/8/ where 8 is the id
 */
const getIdFromUrl = (url: string) => {
    return getDataFromUrl(url)[1];
};

export { getIdFromUrl };

/*
 * returns [ resource, id ] fron url,
 * so return [ 'people', '8' ] from http://swapi.dev/api/people/8/
 */
const getDataFromUrl = (url: string) => {
    const splitted = url.split('/');

    return [
        splitted[splitted.length - 3],
        splitted[splitted.length - 2],
    ];
};

export { getDataFromUrl };

/*
 * returns array with columns structure from columns list
 */
const getColumns = (columns: string[]) => {
    return columns.map((key) => ({
        id: key,
        // default image width is 200px - so set width of column to image width
        width: key === 'images' ? '232px' : undefined,
        translation: makeTranslation(key),
    }))
};

export { getColumns };

/*
 * quick simple solution for translating headers
 * TODO create translation module and use tokens everywhere in application
 */
const makeTranslation = (token: string): string => token.split('_').join(' ');

export { makeTranslation };

/*
 * get data sorted by direction
 * NOTICE temporary solution, may not work properly,
 * sort should be done by server side
 */
const getSorted = ({ column, body, direction }: { column: string, body: BodyConfig[], direction: SortDirection }): BodyConfig[] => {
    // hack because of sort is made on UI part
    const intColumns = [ 'height', 'mass', 'crew', 'age' ];

    return orderBy(body, (entry) => intColumns.includes(column) ? parseInt(entry[column], 10) : entry[column], [ direction ]);
};

export { getSorted };

const mapBodyEntryToFetched = (v, key, availableWithData, fetchedValues) => {
    if (!availableWithData.includes(key)) {
        return v;
    }

    // we need always value as array
    const value = Array.isArray(v) ? v : [ v ];

    const found = fetchedValues.find(entry => Array.isArray(entry[key]));

    if (!found || !found[key]) {
        return value;
    }

    const externalValue = found && found[key];

    return value.map((url) => {
        return externalValue.find(entry => entry.url === url);
    });
};

export default {
    getPaginationData,
    getSorted,
    makeTranslation,
    getColumns,
    getDataFromUrl,
    getIdFromUrl,
    mapBodyEntryToFetched,
};