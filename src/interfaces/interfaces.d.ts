export type SortDirection = 'asc' | 'desc';

export type CellRenderer = (bodyEntry: BodyConfig, columnEntry: ColumnsConfig) => JSX.Element;

export type CellRenderers = {
    [column: string]: CellRenderer
};

export interface ColumnsConfig {
    id: string;
    width?: string;
    translation?: string;
}

export interface BodyConfig {
    [key: string]: any;
}

export interface SortConfig {
    [column: string]: SortDirection;
}

export interface TableConfig {
    columns: ColumnsConfig[] | [];
    body: BodyConfig[] | [];
    pinnedColumn?: string;
    sort?: SortConfig;
    sortHandler?: (column: string, direction: SortDirection) => void;
    pinHandler?: (column: string) => void;
    cellRenderers?: CellRenderers;
    pagination?: PaginationConfig;
}

export interface HeaderCellProps {
    pinHandler?: (column: any) => void;
    clickHandler?: (column: any) => void;
    columnEntry: ColumnsConfig;
    sort?: SortConfig;
    pinnedColumn?: string;
    cellRenderers?: CellRenderers;
    hasClickHandler?: boolean;
}

export interface TableConfigProps {
    resource: string;
    columns?: string[];
    withData?: string[];
    cellRenderers?: CellRenderers;
    config?: TableConfig;
}

export interface GetTableConfigProps extends TableConfigProps {
    columns: string[];
}

export interface GetTableConfigForRowProps extends GetTableConfigProps {
    id: string;
}

export interface GetBodyWithDependenciesProps {
    body: BodyConfig[];
    columns: string[];
    withData?: string[];
}

export interface APIResponse {
    results: BodyConfig[];
    previous: string | null;
    next: string | null;
    count: number;
}

export interface PaginationConfig {
    totalItems: number;
    nextPage: number | null;
    prevPage: number | null;
    perPage: number;
    onPageChange?: (page: number) => void;
}