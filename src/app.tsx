import React from 'react';
import styled, { createGlobalStyle } from 'styled-components'
import { BrowserRouter as Router } from 'react-router-dom';

import Content from 'modules/content/content';
import Menu from 'components/menu/menu';

const GlobalStyle = createGlobalStyle`
    body {
       margin: 0;
       background: whitesmoke;
       font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
        'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
        sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
`;

const FlexDiv = styled.div`display: flex;`;

function App() {
    return (<>
        <GlobalStyle />
        <FlexDiv>
            <Router>
                <Menu />
                <Content />
            </Router>
        </FlexDiv>
    </>);
}

export default App;
