import Utils from '../src/utils/utils';

test('Test Utils -> getIdFromUrl', () => {
    expect(
        Utils.getIdFromUrl('http://swapi.dev/api/people/8/')
    ).toBe('8');

    // NOTICE - structure in url change so function returns
    // incorrect value
    expect(
        Utils.getIdFromUrl('swapi.dev/api/people/8')
    ).toBe('people');
});


test('Test Utils -> getDataFromUrl', () => {
    expect(
        Utils.getDataFromUrl('http://swapi.dev/api/people/8/')
    ).toEqual(['people', '8']);
});


test('Test Utils -> getColumns', () => {
    expect(
        Utils.getColumns([ 'id', 'name', 'day_of_birth' ])
    ).toEqual([
        { id: 'id', translation: 'id' },
        { id: 'name', translation: 'name' },
        { id: 'day_of_birth', translation: 'day of birth' },
    ]);
});


test('Test Utils -> getSorted', () => {
    const data = {
        column: 'name',
        body: [
            { id: '1', name: 'Darth Vader', age: '43' },
            { id: '2', name: 'Owen Lars', age: '34' },
            { id: '3', name: 'Beru Whitesun lars', age: '57' },
        ],
        direction: 'asc',
    };

    const ascSortedData = [
        { id: '3', name: 'Beru Whitesun lars', age: '57' },
        { id: '1', name: 'Darth Vader', age: '43' },
        { id: '2', name: 'Owen Lars', age: '34' },
    ];

    expect(
        Utils.getSorted(data)
    ).toEqual(ascSortedData);

    data.direction = 'desc';

    const descSortedData = [
        { id: '2', name: 'Owen Lars', age: '34' },
        { id: '1', name: 'Darth Vader', age: '43' },
        { id: '3', name: 'Beru Whitesun lars', age: '57' },
    ];

    expect(
        Utils.getSorted(data)
    ).toEqual(descSortedData);
});


test('Test Utils -> getPaginationData', () => {
    const data = {
        count: 88,
        previous: null,
        next: 'http://swapi.dev/api/people/?page=2',
        results: [ { id: '1', name: 'Darth Vader', age: '35' } ],
    };

    expect(
        Utils.getPaginationData(data)
    ).toEqual({
        totalItems: 88,
        nextPage: 2,
        prevPage: null,
        perPage: 10, // hardcoded in utils for now
    });

    const secondData = {
        count: 3,
        previous: 'http://swapi.dev/api/people/?page=3',
        results: [ { id: '1', name: 'Darth Vader', age: '35' } ],
    };

    expect(
        Utils.getPaginationData(secondData)
    ).toEqual({
        totalItems: 3,
        nextPage: null,
        prevPage: 3,
        perPage: 10, // hardcoded in utils for now
    });
});


test('Test Utils -> mapBodyEntryToFetched', () => {
    const homeworld = {
        name: "Naboo",
        rotation_period: "26",
        orbital_period: "312",
        diameter: "12120",
        climate: "temperate",
        gravity: "1 standard",
        terrain: "grassy hills, swamps, forests, mountains",
        surface_water: "12",
        population: "4500000000",
        residents: ["http://swapi.dev/api/people/3/", "http://swapi.dev/api/people/21/"],
        films: ["http://swapi.dev/api/films/3/", "http://swapi.dev/api/films/4/", "http://swapi.dev/api/films/5/"],
        created: "2014-12-10T11:52:31.066000Z",
        edited: "2014-12-20T20:58:18.430000Z",
        url: "http://swapi.dev/api/planets/8/",
    };

    const homeWorld2 = {
        name: "Tattooien",
        rotation_period: "26",
        orbital_period: "312",
        diameter: "12120",
        climate: "temperate",
        gravity: "1 standard",
        terrain: "grassy hills, swamps, forests, mountains",
        surface_water: "12",
        population: "4500000000",
        residents: ["http://swapi.dev/api/people/3/", "http://swapi.dev/api/people/21/"],
        films: ["http://swapi.dev/api/films/3/", "http://swapi.dev/api/films/4/", "http://swapi.dev/api/films/5/"],
        created: "2014-12-10T11:52:31.066000Z",
        edited: "2014-12-20T20:58:18.430000Z",
        url: "http://swapi.dev/api/planets/2/",
    };

    const fetchedData = [
        { homeworld: [ homeworld, homeWorld2 ] },
    ];

    expect(
        Utils.mapBodyEntryToFetched(
            'http://swapi.dev/api/planets/8/',
            'homeworld',
            [ 'homeworld' ],
            fetchedData,
        )
    ).toEqual([ homeworld ]);

    expect(
        Utils.mapBodyEntryToFetched(
            'http://swapi.dev/api/planets/8/',
            'homeworld',
            [ 'homeworldWithError' ],
            fetchedData,
        )
    ).toBe('http://swapi.dev/api/planets/8/');

    expect(
        Utils.mapBodyEntryToFetched(
            ['http://swapi.dev/api/planets/8/', 'http://swapi.dev/api/planets/2/'],
            'homeworld',
            [ 'homeworld' ],
            fetchedData,
        )
    ).toEqual([ homeworld, homeWorld2 ]);
});