import React from 'react';
import Pagination from '../src/components/pagination/pagination';
import renderer from 'react-test-renderer';

test('Test pagination component', () => {
    let tree = renderer.create(
        <Pagination
            totalItems={15}
            nextPage={2}
            prevPage={null}
            perPage={10}
        />
    ).toJSON();

    expect(tree).toMatchSnapshot();

    tree = renderer.create(
        <Pagination
            totalItems={35}
            nextPage={5}
            prevPage={3}
            perPage={10}
        />
    ).toJSON();

    expect(tree).toMatchSnapshot();

    tree = renderer.create(
        <Pagination
            totalItems={35}
            nextPage={null}
            prevPage={5}
            perPage={40}
        />
    ).toJSON();

    expect(tree).toMatchSnapshot();
});