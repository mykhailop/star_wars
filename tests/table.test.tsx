import React from 'react';
import Table from '../src/components/table/table';
import { getColumns } from '../src/utils/utils';
import renderer from 'react-test-renderer';

test('Test table component', () => {
    const body = [
        { id: '3', name: 'Beru Whitesun lars', age: '57' },
        { id: '1', name: 'Darth Vader', age: '43' },
        { id: '2', name: 'Owen Lars', age: '34' },
    ];

    const columns = getColumns([ 'id', 'name', 'age' ]);

    const cellRenderers = {
        id: () => (<div>Hello M.P.</div>),
    };

    let tree = renderer.create(
        <Table
            body={body}
            columns={columns}
        />
    ).toJSON();

    expect(tree).toMatchSnapshot();

    tree = renderer.create(
        <Table
            body={body}
            columns={columns}
            pinnedColumn="name"
        />
    ).toJSON();

    expect(tree).toMatchSnapshot();

    tree = renderer.create(
        <Table
            body={body}
            columns={columns}
            sort={{ name: 'asc' }}
            cellRenderers={cellRenderers}
        />
    ).toJSON();

    expect(tree).toMatchSnapshot();
});