const path = require('path');
const webpack = require('webpack');
const entryPath = path.join(__dirname, 'src');
const buildPath = path.join(__dirname, 'build');

const commonConfig = {
    include: entryPath,
    loader: 'babel-loader',
    query: {
        presets: [ '@babel/preset-env', '@babel/preset-react' ],
        plugins: [
            [ '@babel/plugin-proposal-decorators', { legacy: true } ],
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-transform-async-to-generator',
            '@babel/plugin-transform-runtime',
            '@babel/plugin-transform-modules-commonjs',
        ],
    },
};

module.exports = {
    cache: true,
    mode: 'development',
    entry: {
        main: entryPath + '/index.tsx',
    },
    output: {
        path: buildPath,
        filename: '[name].js',
        publicPath: '/',
    },
    devServer: {
        contentBase: path.join(__dirname, 'static'),
        compress: true,
        port: 3000,
        historyApiFallback: true,
    },
    resolve: {
        alias: {
            components: path.join(__dirname, 'src/components'),
            modules: path.join(__dirname, 'src/modules'),
            utils: path.join(__dirname, 'src/utils'),
            interfaces: path.join(__dirname, 'src/interfaces'),
        },
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    module: {
        // apply loaders to files that meet given conditions
        rules: [{
            test: /\.(ts|tsx)$/,
            include: entryPath,
            loader: 'ts-loader',
        }, { 
            test: /\.(png|jpg)$/,
            include: path.join(__dirname, 'static/img'),
            loader: 'url-loader?limit=10000' 
        }],
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
    },
};
